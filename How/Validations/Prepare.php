<?php

$host = 'localhost';
$database = 'users';
$user = 'root';
$pwd = '';

$conn = new PDO("mysql:host=$host;dbname=$database",$user,$pwd);

$username = $_POST['username'];
$password = $_POST['password'];

$result = $conn->prepare("SELECT  * FROM users WHERE username = :username AND password = :password");
$result->execute([
    ':username' => $username ,
    ':password' => $password
]);

// $result = $conn->prepare("SELECT  * FROM users WHERE username = ? AND password = ?");
// $result->execute($username,$password);



print_r($result->fetchAll(PDO::FETCH_ASSOC));