<?php
session_start();
include_once 'Validations.php';

validate([

     'username' => 'required|min:3|max:10' ,
     'password' => 'required|min:3|max:10' ,

]);



if($_SESSION['errors']){
    header('Location:LoginForm.php');
    exit;
}
