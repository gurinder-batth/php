<?php

function filter($var){
    $var = htmlspecialchars($var);
    $var = stripslashes($var);
    $var = trim($var);
    return $var;
}

$host = 'localhost';
$database = 'users';
$user = 'root';
$pwd = '';

$conn = new PDO("mysql:host=$host;dbname=$database",$user,$pwd);

$username = filter($_POST['username']);
$password = filter($_POST['password']);

$result = $conn->prepare(" INSERT INTO users (username,password)  VALUES(:username,:password) ");
$result->execute([
    ':username' => $username ,
    ':password' => $password
]);
