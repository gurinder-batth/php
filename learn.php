<?php

// $students = [

//                     'Gurinder' ,
//                     'Tom' ,
//                     'ABC'

// ];  //Index Array One D

// echo used for String Only
// var_dump() or print_r used for Both

// echo "<pre>";
// var_dump($students);
// echo "</pre>";

// for($i = 0 ; $i < count($students) ; $i++){
//     echo $students[$i];
//     echo "<br>";
// }

// foreach($students as $key => $student){
// echo   $key .   "  " .    $student . "<br>";
// }


// $students = [
        
//      [
//          'Gurinder' , //0
//          'Sangrur' //1
//      ], //0
//      [
//          'Sanjeev' , //0
//          'Ludhiana' //1
//      ], //1
//      [
//          'William' , //0
//          'Ludhiana' //1
//      ], //2

// ];



// for($i = 0 ; $i< count($students); $i++){
//      for($j = 0 ; $j < count($students[$i]) ; $j++){
//              echo $students[$i][$j] . " ";
//      }
//      echo "<br>";
// }

// foreach($students as $student){
//    echo $student[0] . " " . $student[1] . "<br>";
// }


// $students = [
        
//      [
//         "name"  =>   'Gurinder' , 
//         "city" =>   'Sangrur' 
//      ], //0
//      [
//         "name"  =>   'Sanjeev' , 
//         "city"  =>  'Ludhiana' 
//      ], //1
//      [
//         "name"  =>  'William' , 
//         "city"  =>  'Ludhiana' 
//      ], //2

// ];


// foreach($students as $student){
//    echo $student["name"] . " " . $student["city"] . "<br>";
// }
